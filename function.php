<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($nama)
    {
        echo "Halo $nama, selamat datang di jabar coding camp <br>";
    }

    // panggil function kenalkan
    greetings("abduh");
    greetings("bagas");
    greetings("wahyu");
    greetings("abdul");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";

    echo "<br>";
    function kebalik($nama)
    {
        echo strrev("Haloo $nama");
        echo "<br>";
    }

    // panggil function kenalkan
    kebalik("abduh");
    kebalik("bagas");
    kebalik("wahyu");
    kebalik("abdul");
    echo "<h3>Soal No 3 Palindrome </h3>";
    /* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


    // Code function di sini

    // Hapus komentar di bawah ini untuk jalankan code
    // palindrome("civic") ; // true
    // palindrome("nababan") ; // true
    // palindrome("jambaban"); // false
    // palindrome("racecar"); // true


    // PHP code to check for Palindrome number in PHP
    // Function to check for Palindrome
    function Palindrome_Function($input)
    {
        $reverse = strrev($input);
        if ($reverse == $input) {
            echo "$input true <br>";
        } else {
            echo "$input false";
        }
    }
    Palindrome_Function("nababan");
    Palindrome_Function("civic");
    Palindrome_Function("racecar");
    Palindrome_Function("jambu");


    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    /*
    Soal 4
    buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter
    berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik”
    Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar
    sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
    */

    // Code function di sini

    // Hapus komentar di bawah ini untuk jalankan code
    // echo tentukan_nilai(98); //Sangat Baik
    // echo tentukan_nilai(76); //Baik
    // echo tentukan_nilai(67); //Cukup
    // echo tentukan_nilai(43); //Kurang
    $nilai = 67;
    function tentukan_nilai($nilai)
    {
        if ($nilai >= 98) {
            echo "sangat baik";
        } elseif ($nilai >= 76) {
            echo "baik";
        } elseif ($nilai >= 67) {
            echo "cukup";
        } else {
            echo "kurang";
        }
    }
    tentukan_nilai($nilai);
    ?>

</body>

</html>