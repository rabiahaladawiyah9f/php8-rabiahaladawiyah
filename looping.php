<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>

<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3>Soal No 1 Looping I Love PHP</h3>";

    echo "Looping Pertama <br>";
    for ($i = 2; $i <= 20; $i++) {
        if ($i % 2 == 0)
            echo "$i. I Love PHP<br>";
    }
    echo "<br>";
    echo "Looping kedua <br>";
    for ($n = 20; $n >= 2; $n--) {
        if ($n % 2 == 0)
            echo "$n. I Love PHP<br>";
    }
    echo "<h3>Soal No 2 Looping Array Modulo </h3>";
    /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */
    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array numbers: ";
    print_r($numbers);
    // Lakukan Looping di sini
    echo "<br>";
    echo "Array sisa baginya adalah:  ";
    echo "<br>";

    echo "<h3> Soal No 3 Looping Asociative Array </h3>";
    /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 
        */
    $items = [
        ['[id]' => '001', '[name]' => 'Keyboard Logitek', '[price]' => 60000, '[desc]' => 'Keyboard yang mantap untuk kantoran', '[sc]' => 'logitek.jpeg'],
        ['[id]' => '002', '[name]' => 'Keyboard MSI', '[price]' => 300000, '[desc]' => 'Keyboard gaming MSI mekanik', '[sc]' => 'msi.jpeg'],
        ['[id]' => '003', '[name]' => 'Mouse Genius', '[price]' => 50000, '[desc]' => 'Mouse Genius biar lebih pinter', '[sc]' => 'genius.jpeg'],
        ['[id]' => '004', '[name]' => 'Mouse Jerry', '[price]' => 30000, '[desc]' => 'Mouse yang disukai kucing', '[sc]' => 'jerry.jpeg']
    ];
    foreach ($items as $it) {
        echo "Array (id: " . $it['[id]'] . "";
        echo "name: " . $it['[name]'] . "";
        echo "price: " . $it['[price]'] . " ";
        echo "desc: " . $it['[desc]'] . " ";
        echo "sc: " . $it['[sc]'] . " ";
        echo ")<br>";
    }


    // Output: 

    echo "<h3>Soal No 4 Asterix </h3>";
    $star = 5;
    for ($a = $star; $a > 0; $a--) {
        for ($b = $star; $b >= $a; $b--) {
            echo "*";
        }
        echo "<br>";
    }





    ?>
</body>

</html>